TabControl widget for UXApp
===========================

Version 1.1 -- 2020-12-17

Installation
------------

Add to your `composer.json`:

    {
      "require": {
        "uhi67/uxapp-tabcontrol": "*"
      },
    }

## Usage ##
### In Controller ###
```php
		$tabcontrol1 = new \uhi67\tabcontrol\TabControl([
			'page' => $this,
		'class' => 'tab-blue',
		'name' => 'tabcontrol1',
		'pages' => array(
			array('Erőforrások', ''),
			array('Aldomainek', ''),
			array('Jogosultak', ''),
			array('Zónafájl', 'Megmutatja a zónafájlt'),
		),
		'default' => 0,
			'instant' => false // Set to true for client-side paging
	));
	$tabcontrol1->createNode($node_parent); // tabcontrol node is accessible as $tabcontrol1->node
```

### Option members:

- string **$name** -- the name of the tabcontrol, unique on the page or in the session. Also base of the control request variable
- string **$class** -- extra classname for tabcontrol div
- string **$default** -- index of default tab. Default is the first index of pages
- int **$instant** -- 1 means client side operation
- array **$pages** -- id => array(caption, title, enabled, url [, icon, class]) numeric-indexed or associative
- string **$varname** -- Name of the tab control request variable. If not set, it will be  the name. Alfanumeric only!
- string **$current** -- Number or identifier of active tab

### In view ###

Server-side paging:
```
	<xsl:apply-templates select="tabcontrol"/>
	<xsl:choose>
		<xsl:when test="tabcontrol/@current=0">Content-of-page-0</xsl:when>
		<xsl:when test="tabcontrol/@current=1">Content-of-page-1</xsl:when>
	</xsl:choose>
```

Client-side paging (instant is true):
```
	<xsl:apply-templates select="tabcontrol"/>
	...
	<xsl:template match="tab[@id=0]" mode="user-content">
 	Content-of-page-0
 </xsl:template>
	<xsl:template match="tab[@id=1]" mode="user-content">
 	Content-of-page-1
 </xsl:template>
```

Füles vezérlő használati példák
===============================

Kétféleképpen használható: szerver oldali és kliens oldali lapozással.

1. Szerver oldali lapozás
-------------------------

### php kód

```php
//...
$tabs = [
    ['Cím-1', 'tipp-1', $enabled1],
    ['Cím-2', 'tipp-2', $enabled2],
    //...
];

$tabcontrol = new \uhi67\tabcontrol\TabControl([
    'page' => $this, 
    'parent' => $this->node_parent, 
    'name' => $tabname, 
    'pages' => $tabs, 
    'class' => $classname, 
    'default' => $default,
    'instant' => 0
]);
$tab = $tabcontrol->tab;
// A kiválaszottt fül adatainak betöltése
if($tab==1) $this->loadTabData1();
if($tab==2) $this->loadTabData2();
```

### xslt kód

```$xslt
<xsl:apply-templates select="tabcontrol" />
<xsl:choose>
    <xsl:when test="tabcontrol/@current=1"><xsl:call-template name="tabdata1" /></xsl:when>
    <xsl:when test="tabcontrol/@current=2"><xsl:call-template name="tabdata2" /></xsl:when>
    ...
</xsl:choose>
```

2. Kliens oldali lapozás
------------------------
### php kód

```php
$tabs = [
    ['Cím-1', 'tipp-1', $enabled1],
    ['Cím-2', 'tipp-2', $enabled2],
    //...
];

$tabcontrol = new TabControl([
    'page' => $this, 
    'parent' => $this->node_parent, 
    'name' => $tabname, 
    'pages' => $tabs, 
    'class' => $classname, 
    'default' => $default,
    'instant' => true
]);
// Az összes fül adatainak betöltése. Az adat olyan szerkezetű legyen, hogy ne keveredjenek össze.
$this->loadTabData1();
$this->loadTabData2();
```

### xslt kód

Ebben az esetben a tabcontrol/@current értéke mindig $default (1), a tab/@id szerint kell megjeleníteni 
a fülek tartalmát. A lapok rejtését és megjelenítését a modul intézi.

```$xslt
<xsl:apply-templates select="tabcontrol" />

<xsl:template match="tab[@id=1]" mode="user-content">
	<!-- 1. fül adatainak megjelenítése -->
	<xsl:call-template name="tabdata1" />
</xsl:template>

<xsl:template match="tab[@id=2]" mode="user-content">
	<xsl:call-template name="tabdata2" />
</xsl:template>
```

Change log
==========

## 1.1 -- 2020-12-17

- using hidden class for client-side XSLT compatibility
- using uxapp-tooltip class
- logo added

## 1.0 -- 2020-11-03

- first public release
