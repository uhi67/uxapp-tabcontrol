<?php /** @noinspection PhpUnused */

namespace uhi67\tabcontrol;

use Exception;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\Module;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxml\UXMLElement;

/**
 * TabControl module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2017-2020
 * @license MIT
 * @package uxapp-tabcontrol
 */

/**
 * TabControl
 *
 * Features:
 * - with or without form,
 * - client or server side content switching
 *
 * ## Using ##
 *
 * ### In Controller ###
 *
 * ```php
 * 		$tabcontrol1 = new TabControl([
 * 			'page' => $this,
 *			'class' => 'tab-blue',
 *			'name' => 'tabcontrol1',
 *			'pages' => array(
 *				array('Erőforrások', ''),
 *				array('Aldomainek', ''),
 *				array('Jogosultak', ''),
 *				array('Zónafájl', 'Megmutatja a zónafájlt'),
 *			),
 *			'default' => 0,
 * 			'instant' => false // Set to true for client-side paging
 *		));
 *		$tabcontrol1->createNode($node_parent); // tabcontrol node is accessible as $tabcontrol1->node
 * ```
 * ### Option members:
 *
 * - UXAppPage **$page** -- the controller (mandatory)
 * - string **$name** -- the name of the tabcontrol, unique on the page or in the session. Also base of the control request variable
 * - string **$class** -- extra classname for tabcontrol div
 * - string **$default** -- index of default tab. Default is the first index of pages
 * - int **$instant** -- 1 means client side operation
 * - array **$pages** -- id => array(caption, title, enabled, url [, icon, class]) numeric-indexed or associative
 * - string **$varname** -- Name of the tab control request variable. If not set, it will be  the name. Alfanumeric only!
 * - string **$current** -- Number or identifier of active tab
 *
 * ### In view ###
 *
 * Server-side paging:
 * ```
 *		<xsl:apply-templates select="tabcontrol"/>
 *		<xsl:choose>
 *			<xsl:when test="tabcontrol/@current=0">Content-of-page-0</xsl:when>
 *			<xsl:when test="tabcontrol/@current=1">Content-of-page-1</xsl:when>
 *		</xsl:choose>
 * ```
 *
 * Client-side paging (instant is true):
 * ```
 *		<xsl:apply-templates select="tabcontrol"/>
 *
 * 	...
 * 	<xsl:template match="tab[@id=0]" mode="user-content">
 *  	Content-of-page-0
 *  </xsl:template>
 *
 * 	<xsl:template match="tab[@id=1]" mode="user-content">
 *  	Content-of-page-1
 *  </xsl:template>
 * ```
 *
 * @property string $current -- id of current selected tab page. May be set by setCurrent()
 * If not set, taken from the request, default from the session, the last default is the default
 */
class TabControl extends Module {
	/** @var string $name -- the name of the tabcontrol, unique on the page or in the session. Also base of the control request variable */
	public $name;
	/** @var string $divClass -- extra classname for tabcontrol div */
	public $divClass;
	/** @var string $default -- index of default tab. Default is the first index of pages */
	public $default = null;
	/** @var int $instant -- 1 means client side operation */
	public $instant = 0;
	/** @var array $pages -- id => array(caption, title, enabled, url [, icon, class]) numeric-indexed or associative */
	public $pages;
	/** @var string $varname -- Name of the tab control request variable. If not set, it will be  the name. Alfanumeric only! */
	public $varname = null;
	/** @var mixed|null|string $_current is the id of currently selected tab (can be set only after setting pages) */
	private $_current = null;

	/**
	 * ## Tabcontrol constructor.
	 *
	 * You may create it's node in the DOM with createNode() method.
	 *
	 * ### Option members:
	 *
	 * - string **$name** -- the name of the tabcontrol, unique on the page or in the session. Also base of the control request variable
	 * - string **$class** -- extra classname for tabcontrol div
	 * - string **$default** -- index of default tab. Default is the first index of pages
	 * - int **$instant** -- 1 means client side operation
	 * - array **$pages** -- id => array(caption, title, enabled, url [, icon, class]) numeric-indexed or associative
	 * - string **$varname** -- Name of the tab control request variable. If not set, it will be  the name. Alfanumeric only!
	 * - string **$current** -- Number or identifier of active tab
	 *
	 * @throws Exception
	 * @return void
	 */
	function prepare() {
		parent::prepare();
		if(!$this->name) $this->name = Util::randStr(8);
		if(!$this->varname) $this->varname = Util::toNameID($this->name, '_', '');
		if(!$this->pages) return; // Only register // TODO: registerStatic átgondolása után itt Exception lesz
		if(!$this->default || !array_key_exists($this->default, $this->pages)) $this->default = min(array_keys($this->pages));

		$default = $this->instant ? $this->default : $this->page->app->session->get($this->varname, $this->default);
		if(!$this->_current) $this->_current = $this->page->app->request->req($this->varname, $default);

		/** @var array $indices -- indices of enabled pages */
		$indices = array_keys(array_filter($this->pages, function($p) {return ArrayUtils::getValue($p, 2, 1);} ));
		$all = array_merge($indices, array_keys($this->pages)); // active + all indices (to be sure first exists)
		// If current page is not enabled, the first enabled page will be active (or the first disabled, if none)
		if(!isset($this->pages[$this->_current]) || !ArrayUtils::getValue($this->pages[$this->_current], 2, 1)) {
			$this->_current = $all[0];
		}
		$_SESSION[$this->varname] = $this->_current;
	}

	/**
	 * Creates a tabcontrol node in the parent node
	 *
	 * @param UXMLElement $node_parent
	 * @param boolean $standalone -- Has it's own &lt;form>. Set to false if you want to insert into another existing form.
	 *
	 * @return UXMLElement -- the created node
	 * @throws UXAppException
	 * @throws Exception
	 */
	function createNode($node_parent=null, $standalone=true) {
		$node_parent = $this->parentNode($node_parent);
		$node = $node_parent->addNode( 'tabcontrol', [
			'name' => $this->name,				// may contain .-s
			'varname' => $this->varname,		// must not contain .-s
			'class' => $this->divClass,
			'current' => $this->_current,
			'instant' => (int)$this->instant,
			'standalone' => (int)$standalone,
			'focus' => $this->page->app->request->getInt('focus_'.$this->varname)
		]);
		foreach($this->pages as $i=>$page) {
			$node->addNode( 'tab', [
				'id' => $i,
				'caption' => ArrayUtils::getValue($page, 'caption', ArrayUtils::getValue($page, 0, 'page_'.$i)),
				'title' => ArrayUtils::getValue($page, 'title', ArrayUtils::getValue($page, 1, '')),
				'enabled' => ArrayUtils::getValue($page, 'enabled', ArrayUtils::getValue($page, 2, 1)) ? 1 : 0,
				'url' => ArrayUtils::getValue($page, 'url', ArrayUtils::getValue($page, 3)),
				'icon' => ArrayUtils::getValue($page, 'icon'),
				'class' => ArrayUtils::getValue($page, 'class')
			]);
		}
		return $this->node = $node;
	}

	/**
	 * Mandatory for Models
	 */
	function action() {}

	/**
	 * Sets the actual page in tabcontrol.
	 * If requested page does not exist, returns false
	 *
	 * @param int $tab
	 *
	 * @return boolean -- success
	 */
	function setCurrent($tab) {
		UXApp::trace(['current', $tab]);
		if(!array_key_exists($tab, $this->pages)) return false;
		$this->_current = $tab;
		UXApp::trace(['current', $tab]);
		if($this->node) $this->node->setAttribute('current', $tab);
		return true;
	}

	/**
	 * @return mixed|null|string
	 */
	function getCurrent() {
		return $this->_current;
	}
}
